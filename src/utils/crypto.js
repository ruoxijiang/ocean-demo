const CryptoJS = require("crypto-js");
const key = 'bla'

// Encrypt
export function encrypter(str = '', secret = key) {
    return CryptoJS.AES.encrypt(str.toString(), secret)
}

// Decrypt
export function decrypter(str = '', secret = key) {
    let bytes = CryptoJS.AES.decrypt(str.toString(), secret);
    return bytes.toString(CryptoJS.enc.Utf8)
}

export function hmac_header(contentMd5 = "", requestDate = "", apiPath = "", secret_key = "") {
    const hexString = CryptoJS.HmacSHA1(contentMd5 + "\n" + requestDate + "\n" + apiPath, secret_key);
    return hexString.toString(CryptoJS.enc.Base64);
}

export function md5_header(content = {}) {
    let message = "";
    let i = 0;
    if (content) {
        for (let data in Object.values(content)) {
            if (i === 0) {
                message = data;
                i++;
            } else {
                message = message + "\n" + data;
            }
        }
    }
    let md5_string = CryptoJS.MD5(message);
    return md5_string.toString(CryptoJS.enc.Base64);
}

export function requestDate_header() {
    // return "Thu, 18 Oct 2018 00:59:35 GMT";
    let d = new Date();
    return d.toUTCString()
}
