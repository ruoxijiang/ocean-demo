import {requestDate_header, hmac_header, md5_header} from "./crypto";
export const encrypted_get=({data, api_path}, dispatch)=>{
    let options={data, api_path};
    options.method = 'get';
    return encrypted_fetch(options, dispatch);
};

export const encrypted_post=({data, api_path}, dispatch)=>{
    let options={data, api_path};
    options.method = 'post';
    return encrypted_fetch(options, dispatch);
};

const encrypted_fetch = (options={method:'get',data: null, api_path: ""}, dispatch) => {
    let {
        method = 'get',
        data=null,
        api_path
    } = options;
    let headers = {platform: 3, accessKey: window.sessionStorage.getItem('publicKey'), credentials: 'include'};
    let fetchOptions = {method: method};
    const requestDate = requestDate_header();
    headers.requestDate = requestDate;
    if (method.toLocaleLowerCase() === 'post') {
        if (data) {
            let formData = Object.keys(data).map((key)=>{
                return `${key}=${encodeURIComponent(data[key])}`
            }).join("&");
            headers['Content-Type'] = 'application/x-www-form-urlencoded';
            fetchOptions.body = formData;
        }
    } else if (method.toLocaleLowerCase() === 'get') {
        if (data) {
            let keys= Object.keys(data);
            for (let i=0; i<keys.length; i++) {
                headers[keys[i]] = data[keys[i]];
            }
        }
    }
    const md5Header = md5_header(data);
// eslint-disable-next-line no-undef
    const service_path = global.apiBase + api_path;
    const url = api+ api_path;
    const hmacHeader = hmac_header(md5Header, requestDate, service_path, window.sessionStorage.getItem('privateKey'));
    headers.contentMD5 = md5Header;
    headers.hmac = hmacHeader;
    fetchOptions.headers = headers;
    return fetch(url, fetchOptions).then(response => {
        return checkStatus(response, dispatch);
    }, response => checkNetwork(response.json(), dispatch));
};
export function actionCreator(type, data = {}) {
    return Object.assign({type: type}, {...data})
}

function checkStatus(response, dispatch) {
    const statusCode= response.statusCode;
    if (statusCode==='null'){
        return Promise.resolve(response);
    }else if (statusCode===800) {
        if(response.hasOwnProperty('returnObj')){
            return Promise.resolve(response.returnObj)
        }else{
            console.warn("Backend return have no 'returnObj'");
            return Promise.resolve(response);
        }
    }else if(statusCode === 900){
        if(response.hasOwnProperty('message')){
            let err="";
            if(typeof response.message === 'string' && response.message.includes("{")!==-1){
                response.message = response.message.replace(/;/g,"").replace(/\\/g,"").replace(/\"\{/g, "{").replace(/\}\"/g, "}");
            }
            try{
                err = JSON.parse(response.message);
            }catch(e){
                console.warn("Parse error message failed");
                err = response.message;
            }
            return Promise.reject(err);
        }else if(response.hasOwnProperty('returnObj')){
            return Promise.reject(response.returnObj);
        }
        console.warn('no message data found');
        return Promise.reject({'message': 'Generic Error'});
    }else{
        console.error("api request invalid", response);
        throw 'Unknown Error';
    }
}

function checkNetwork(response, dispatch) {
    console.error("checkNetwork: ", response)
    const error = new Error('Error')
    throw error
}
export default fetch;
