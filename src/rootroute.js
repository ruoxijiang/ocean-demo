import React from 'react';

const rootRoute = [{
    path: '/',
    component: require('./components/portal/components/appPortal').default,
    childRoutes: [
        // require('./components/login'),
        require('./routes'),
        {
            path: 'error', //
            component: require('./commonComponents/notFound/index').default,
        }, {
            path: '*',
            component: require('./components/portal/components/appPortal').default,
        }
    ]
}]
export default rootRoute
