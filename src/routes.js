module.exports = {
    path: 'navroute',
    getChildRoutes(partialNextState, cb) {
        require.ensure([], (require) => {
            cb(null, [
                require('./components/serviceList/vpcPortal/index')
            ])
        })
    },
    getComponent(nextState, cb) {
        require.ensure([], (require) => {
            cb(null, require('./components/portal/components/homePage').default)
        }, 'navroute')
    }
};