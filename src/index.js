import "babel-polyfill";
import React from 'react';
import {render} from "react-dom";
import {browserHistory} from "react-router";
import {syncHistoryWithStore} from "react-router-redux";
import './config';
import App from './components/App';
import configureStore from "./store/configureStore";
import useBasename from "history/lib/useBasename";
require("./style/common/common.scss");

const store = configureStore();
const history = syncHistoryWithStore(browserHistory, store);


if (process.env.NODE_ENV === 'production') {
    require('./style/common/antd.pro.less')
    render(
        <App store={store} history={useBasename(() => history)({basename: process.env.PUBLIC_URL})}/>,
        document.getElementById('root')
    )
} else {
    require('./style/common/antd.less');
        sessionStorage.setItem('privateKey', 'hahaha');
        sessionStorage.setItem('publicKey', 'lalala');
        sessionStorage.setItem('publicCloudAddress', 'https://www.google.com');
    render(
        <App store={store} history={useBasename(() => history)({basename: `/`})}/>,
        document.getElementById('root')
    )
}
// registerServiceWorker();
