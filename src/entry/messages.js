import { defineMessages } from 'react-intl';
const messages = defineMessages({

    vpcStatusOK:{
        id: 'vpc.status.OK',
        defaultMessage: "OK"
    },
});

export default messages;