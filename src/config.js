global.host =  window.location.protocol+'//'+window.location.host;
global.apiBase='/apiproxy/v3/';
global.api = window.location.protocol+'//'+window.location.host+ '/apiproxy/v3/';

global.locale = 'zh-cn';

if(process.env.NODE_ENV === 'production'){
  global.basename = process.env.PUBLIC_URL
}else{
  global.basename = ''
}

global.throwFetchedError=(error,response)=>{
	if (Object.prototype.toString.call(response) == '[object Object]') {
		error.message = response
	}
  throw error
}

const Modal = require('antd/lib/modal')
const info = Modal.info
global.checkStatus = function(response,dispatch) {
  const headers = response.headers
  const authenticate = headers.get('www-authenticate')
  if(authenticate && authenticate == 'Basic realm="Bla Api"' && window.location.href.indexOf('/login') === -1){
    sessionStorage.removeItem("username")
    sessionStorage.removeItem("password")
    info({
      title: 'login failed！',
      onOk: () => {
        window.location.href = window.location.protocol+'//'+window.location.host+`${global.basename}/login`
      },
    });
  }
  if(response.status >= 200 && response.status < 300){
    return response
  }
  let error
  if(response.status === 403 || response.status === 401){
    error = new Error('Access denied')
    global.throwFetchedError(error,response)
  }else if (response.status >= 500) {
    error = new Error(response.status)
    global.throwFetchedError(error,response)
  }else {
    let decoder = new TextDecoder()
    let reader = response.body.getReader()
    return reader.read().then(function processResult(result) {
      if(result.done) return
      let responseText = decoder.decode(result.value, {stream: true})
      let responseObj = JSON.parse(responseText)
      error = new Error(responseObj.collection[0].message)
      global.throwFetchedError(error,response)
      //return reader.read().then(processResult)
    })
  }
}
global.checkNetwork = function(dispatch){
  const error = new Error('Check Network')
  throw error
}
global.parseJSON = function(response) {
  try{
    if(response.status == 204){
      return response
    }else{
      return response.json()
    }
  }catch(e){
    return response
  }
}

const oldFetchfn = require('isomorphic-fetch')
global.fetch = function(input, opts, dispatch){
  // opts.credentials = 'include'
    !!sessionStorage.authorization && (opts.Authorization = sessionStorage.authorization);

  opts.timeout = '30000'
  let fetchPromise = oldFetchfn(input, opts).then(response=>global.checkStatus(response,dispatch),response=>global.checkNetwork(response,dispatch)).then(global.parseJSON)
  var timeoutPromise = new Promise(function(resolve, reject){
   setTimeout(()=>{
     reject(new Error("Request time out"))
   }, opts.timeout)
   });
  return Promise.race([fetchPromise, timeoutPromise])
}

global.makeCancelable = (promise) => {
	let hasCanceled_ = false;

	const wrappedPromise = new Promise((resolve, reject) => {
		promise.then((val) =>
			hasCanceled_ ? reject({isCanceled: true}) : resolve(val)
		);
		promise.catch((error) =>
			hasCanceled_ ? reject({isCanceled: true}) : reject(error)
		);
	});

	return {
		promise: wrappedPromise,
		cancel() {
			hasCanceled_ = true;
		},
	};
};

global.clientWidth = document.documentElement.clientWidth
global.clientHeight = document.documentElement.clientHeight
