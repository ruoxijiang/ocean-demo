import React, {Component} from "react";
import {connect} from "react-redux";
import {Layout} from "antd";
import ErrorBoundary from 'common/errorBoundary/ErrorBoundary';
const {Content} = Layout;

const style = require('../../../style/app.scss');

class HomePage extends Component {
    constructor(props, context) {
        super(props, context)

        this.state = {
            menuName: '',
            parentMenuName: '',
            AllServicesComponent: null,
            MyHeaderComponent: null
        }
    }

    componentWillMount() {
        import("components/serviceList/components/allServices.js").then(AllServices => {
            this.setState({AllServicesComponent: AllServices.default});
        });
        import("./myHeader").then(MyHeader => {
            this.setState({MyHeaderComponent: MyHeader.default});
        });
    }

    render() {
        const {MyHeaderComponent, AllServicesComponent} = this.state;
        return (
            <Layout className={style.homePage}>
                {MyHeaderComponent ? <MyHeaderComponent/> : null}
                <Layout className={style.panel} style={{height: '90%'}}>
                    <Content className={style.main}>
                        <ErrorBoundary>
                            {this.props.children || (AllServicesComponent ? <AllServicesComponent/> : null)}
                        </ErrorBoundary>
                    </Content>
                </Layout>
            </Layout>
        )
    }
}

function mapStateToProps(state, ownProps) {
    return {}
}

export default connect(mapStateToProps)(HomePage);
