import React, {Component} from "react";
import {Layout} from "antd";
import {connect} from "react-redux";
import {Link} from "react-router";

const style = require('../../../style/app.scss');

const {Header} = Layout;

class MyHeader extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return <Header id="header" className={style.header}>
            <div className={style.logoContainer}>
                <img src={`${global.basename}/images/logosvg.svg`} className={style.logo}/>
            </div>
            <div className={'header-content ' + style.headerContent}>
                <Link to="/navroute">Console</Link>
            </div>
        </Header>
    }
}

function mapStateToProps(state, ownProps) {
    return {}
}

export default connect(mapStateToProps)(MyHeader);