import React, {Component} from "react";
import HomePage from "./homePage";
const style = require('../../../style/app.scss');
class AppPortal extends Component {

  constructor(props) {
    super(props);

    this.state = {};
  }
  render() {
      return <div className={style.app}>
          {this.props.children || <HomePage/>}
      </div>
  }
}
export default AppPortal;