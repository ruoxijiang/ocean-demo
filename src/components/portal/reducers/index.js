import {combineReducers} from "redux";
import {routerReducer as routing} from "react-router-redux";
import {publicData} from 'components/serviceList/reducers';
export const SELECT_MENU_KEY = "SELECT_MENU_KEY";
function selectMenuKey(state = {
    menuKey: 'overview'
}, action) {
    switch (action.type) {
        case SELECT_MENU_KEY:
            return Object.assign({
                ...state,
                menuKey: action.menuKey
            })
            break
        default:
            return state
    }
}

const rootReducer = combineReducers({
    routing,
    selectMenuKey,
    publicData
})

export default rootReducer
