import * as types from './actions';

function ecsClicked(state) {
    return {ecsClickedCount: state.ecsClickedCount + 1};
}

function templateClicked(state, isError) {
    return {templateClickedCount: state.templateClickedCount + 1};
}

export function publicData(state = {
    ecsClickedCount: 0,
    templateClickedCount: 0
}, action) {
    let tmp = {};
    switch (action.type) {
        case types.ECS_CLICKED:
            tmp = ecsClicked(state);
            break;
        case types.TEMPLATED_CLICKED:
            tmp = templateClicked(state, action.onError);
            break;
        default:
            return state;
    }
    return {...state, ...tmp};
}