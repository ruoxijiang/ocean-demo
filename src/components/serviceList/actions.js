import {encrypted_get, actionCreator} from "utils/request";
export const ECS_CLICKED = "ECS_CLICKED";
export const TEMPLATED_CLICKED = "TEMPLATED_CLICKED";

export const ecsClicked = ()=>{
    return {type: ECS_CLICKED, data: 1};
};
export const templatedClicked = () => {
    let data = {
        google: 'yeah'
    };
    return dispatch =>{
        encrypted_get({data: data, api_path: "queryPrivateIp"})
            .then((data)=>{
                dispatch(actionCreator(TEMPLATED_CLICKED, {onError: false}))
            })
            .catch(err => {
                dispatch(actionCreator(TEMPLATED_CLICKED, {onError: true}))
            })
    }

};