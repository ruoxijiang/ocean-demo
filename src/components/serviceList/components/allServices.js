
import React, {Component} from 'react';
import {connect} from "react-redux";
import {browserHistory} from "react-router";
import cloudHostIcon from 'style/img/cloudHostIcon.png';
import {slectMenuKey} from "components/portal/actions";
import {Spin} from 'antd'
import {ecsClicked, templatedClicked} from "components/serviceList/actions";

const style = require('style/allServices.scss');

export class AllServices extends Component {
    constructor() {
        super();
        this.state = {
            isLeaving: false
        }
    }


    onMenuClick = (parentPath, type) => {
        this.setState({isLeaving: true});
        this.props.dispatch(slectMenuKey(type));
        if(type==='bandwidth'){
            this.props.dispatch(templatedClicked());
        }else{
            this.props.dispatch(ecsClicked());
        }
        browserHistory.push(`${basename}/navroute/${parentPath}/${type}`);
    }

    render() {
        const {templateClickCount, ecsClickedCount} = this.props;
        return <div className={style.allService}>
            <ul>
                <li>
                    <div className={style.title}>Demo</div>
                    <Spin spinning={this.state.isLeaving}>
                        <div className={style.box}
                             onClick={() => this.onMenuClick('vdcPortal', 'vdc')}>
                            <img className={style.icon} src={cloudHostIcon}/>
                            <span className={style.content}>
							<div className={style.name}>ECS Clicked（{ecsClickedCount || 0}）</div>
							<div className={style.desc}>Times ECS Clicked</div>
						</span>
                        </div>
                    </Spin>
                    <Spin spinning={this.state.isLeaving}>
                        <div className={style.box}
                             onClick={() => this.onMenuClick('vdcPortal', 'bandwidth')}>
                            <img className={style.icon} src={cloudHostIcon}/>
                            <span className={style.content}>
							<div className={style.name}>Template Clicked（{templateClickCount || 0}）</div>
							<div className={style.desc}>Times Template Clicked</div>
						</span>
                        </div>
                    </Spin>
                </li>
            </ul>
        </div>
    }
}

function mapStateToProps(state, ownProps) {
    const {publicData} = state;
    return Object.assign({}, ownProps, {
        templateClickCount: publicData.templateClickedCount,
        ecsClickedCount: publicData.ecsClickedCount
    });
}
export default connect(mapStateToProps)(AllServices);
