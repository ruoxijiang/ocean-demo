module.exports = {
	path: 'vdcPortal',
	getChildRoutes(partialNextState, cb) {
		require.ensure([], (require) => {
			cb(null, [
				require('./vpc/vpcRoute'),
				require('./bandwidth/bandwidthRoute')
			])
		})
	},
	getComponent(nextState, cb) {
		require.ensure([], (require) => {
			cb(null, require('./components/vdcPortal').default)
		}, 'vdcPortal')
	}
};