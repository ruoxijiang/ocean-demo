import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import {connect} from "react-redux";
import {Link, browserHistory} from "react-router";
import {Layout, Menu,Spin} from 'antd';
import cloudHostConsole from 'style/img/cloudHostConsole.png';
import {slectMenuKey} from 'components/portal/actions';

const {Sider, Content} = Layout;
const style = require('style/cloudHostPortal.scss');

class VdcPortal extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            selectMenuKey: props.menuKey ? props.menuKey : 'vdc',
            isShow:false
        };
    }

    componentWillMount() {
        // if((this.props.menuKey!=='vdc')||(this.props.menuKey!=='securityGroup')||(this.props.menuKey!=='bandwidth')){
        //     this.props.dispatch(slectMenuKey('vdc'));
        // }
    }
    componentWillReceiveProps(nextProps) {

        if (this.props.location.pathname!==nextProps.location.pathname){
            this.setState({
                isShow:false
            });
        }
    }

    menuClick = (item) => {
        if (this.props.menuKey !== item.key) {
            this.props.dispatch(slectMenuKey(item.key));
        }
        this.setState({
            isShow:true
        })
    };

    render() {
        return <Layout className={style.cloudHostPortal}>
            <Sider width={240} className={`${style.left} my-slider`}>
                <div>
                    <img className={style.logo} src={cloudHostConsole}/>
                    <div className={style.logoText}>ECS</div>
                </div>
                <Menu
                    className={style.navLeftMenu}
                    mode="horizontal"
                    theme="dark"
                    onClick={this.menuClick}
                    defaultSelectedKeys={[this.props.menuKey]}
                    selectedKeys={[this.props.menuKey]}
                >
                    <Menu.Item key="overview">
                        <Link to="/navroute/">Console</Link>
                    </Menu.Item>
                    <Menu.Item key="vdc">
                        <Link to="/navroute/vdcPortal/vdc">ecs</Link>
                    </Menu.Item>
                    <Menu.Item key="bandwidth">
                        <Link to="/navroute/vdcPortal/bandwidth">Bandwidth</Link>
                    </Menu.Item>
                </Menu>
            </Sider>
            <Content className={`cloudHostContent ${style.cloudHostContent}`}>
                <Spin spinning={this.state.isShow}>
                {this.props.children}
                </Spin>
            </Content>
        </Layout>
    }
}

function mapStateToProps(state, ownProps) {
    const {selectMenuKey} = state;
    return Object.assign({}, ownProps, {
        menuKey: selectMenuKey.menuKey
    });
}

export default connect(mapStateToProps)(VdcPortal);