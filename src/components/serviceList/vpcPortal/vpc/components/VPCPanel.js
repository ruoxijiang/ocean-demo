import React, {PureComponent} from 'react';
import {connect} from "react-redux";
import {injectIntl} from 'react-intl';
import VPC from './VPC';

export class VPCPanel extends PureComponent {
    constructor() {
        super();
        this.state = {};
    }

    render() {
        return <div style={{height: '100%'}}>
            {this.props.children || <VPC/>}
        </div>
    }
}

function mapStateToProps(state, ownProps) {
    return {};
}

export default connect(mapStateToProps)(injectIntl(VPCPanel));