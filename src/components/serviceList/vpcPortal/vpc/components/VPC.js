import React, {PureComponent} from "react";
import {connect} from "react-redux";
import {injectIntl} from 'react-intl';
import helpIcon from 'style/img/helpIcon.png';
import style from 'style/cloudVPC.scss';

export class VPC extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {}
    }

    renderHeader() {
        return <div className={'panel-header'}>
            <span className={`panel-title`}>VPC Clicked({this.props.vpcClickedCount})<img className={`helpIcon`} src={helpIcon}/></span>
        </div>
    }

    renderVPCs() {
        return <div className={style.cloudVPC}>
            <div style={{flex: '1', display: 'flex', flexDirection: 'column'}}>
                <div className={`tableList balance ${style.tableList}`} id="vpcs-scroll-box">
                </div>
            </div>
        </div>
    }

    render() {
        return <div className={style.diskContent}>
            {this.renderHeader()}
            {this.renderVPCs()}
        </div>
    }
}

VPC.defaultProps = {
    regionId: 'cn-gzT',
    zoneId: 'cn-gzTa'
};

function mapStateToProps(state, ownProps) {
    const {publicData} = state;
    return Object.assign({}, ownProps, {
        vpcClickedCount: publicData.ecsClickedCount
    });
}

export default connect(mapStateToProps)(injectIntl(VPC));