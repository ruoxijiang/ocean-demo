module.exports = {
    path: 'bandwidth',
    getChildRoutes(partialNextState, cb) {
        require.ensure([], (require) => {
            cb(null, [
            ])
        })
    },
    getComponent(nextState, cb) {
        require.ensure([], (require) => {
            cb(null, require('./components/BandwidthPanel').default)
        }, 'bandwidthPanel')
    }
};