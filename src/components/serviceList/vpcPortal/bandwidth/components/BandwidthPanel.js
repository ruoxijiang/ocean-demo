import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import {connect} from "react-redux";
import {injectIntl} from 'react-intl';
import Bandwidth from './Bandwidth';

export class BandwidthPanel extends PureComponent {
    constructor() {
        super();
        this.state = {};
    }

    render() {
        return <div style={{height: '100%'}}>
            {this.props.children || <Bandwidth/>}
        </div>
    }
}

function mapStateToProps(state, ownProps) {
    return {};
}

export default connect(mapStateToProps)(injectIntl(BandwidthPanel));