import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect, Provider} from 'react-redux'
import route from '../rootroute'
import {Router} from 'react-router'
import {LocaleProvider, notification} from 'antd';
import {addLocaleData, IntlProvider} from 'react-intl';
import en_US from '../entry/en-US'
import en from 'react-intl/locale-data/en'

notification.config({
    placement: 'bottomRight',
    bottom: 50,
    duration: 10,
})
addLocaleData([...en]);

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            locale: 'EN'
        };
    }

    componentWillMount() {
        this.chooseLocale()
    }

    chooseLocale = () => {
        const {language = 'EN'} = this.props;
        if (language === 'EN') {
            return en_US;
        } else {
            return en_US;
        }
    }

    render() {
        const {store, history} = this.props;
        return <LocaleProvider locale={this.chooseLocale().antd}>
            <IntlProvider locale={this.chooseLocale().locale} messages={this.chooseLocale().messages}>
                <Provider store={store}>
					<Router history={history} routes={route}/>
                </Provider>
            </IntlProvider>
        </LocaleProvider>
    }
}

function mapStateToProps(state, ownProps) {
    return {
        language: 'EN',
    }
}

App.propTypes = {
    store: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired
};

export default  connect(mapStateToProps)(App);
